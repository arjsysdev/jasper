<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PHPJasper\PHPJasper;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function jrpt($id = 0)
    {
        
        $outputFile = public_path('js/ViewerJS');
        $options = [
            'format' => ['pdf'],
            'params' => [
                'byID' => $id
            ],
            'db_connection' => [
                'driver' => env('RPT_CONNECTION'),
                'username' => env('RPT_USERNAME'),
                'password' => env('RPT_PASSWORD'),
                'host' => env('RPT_HOST'),
                'database' => env('RPT_DATABASE'),
                'port' => env('RPT_PORT')
            ]
        ];
        $jasper = new PHPJasper;
        $inputFile = public_path('js/ViewerJS/reportjasper.jasper');
        
        $jasper->process(
            $inputFile,
            $outputFile,
            $options
        )->execute();
        
        return view('jasper');
    }
}

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Document</div>

                <div class="card-body">
                    <iframe src="{{ url('js/ViewerJS/reportjasper.pdf') }}" frameborder="0" width="704" height="1024"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
